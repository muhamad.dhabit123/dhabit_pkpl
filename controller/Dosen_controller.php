<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dosen_model');
        $this->load->model('Link_model');
        if (!$this->session->userdata('username')) {
            redirect(base_url(''));
        }
    }

    public function index()
    {
        $data = array(
            'data' => $this->Dosen_model->getAll(),
        );
        $this->load->view('dosen/dosen_view', $data);
    }

    public function addDosenView()
    {
        $this->load->view('dosen/tambahDosen_view');
    }

    public function addDosen()
    {
        $nama = $this->input->post('nama');
        $niy = $this->input->post('niy');
        $email = $this->input->post('email');

        $array_data = array(
            'nama_dosen' => $nama,
            'niy' => $niy,
            'email' => $email,
        );

        $this->Dosen_model->tambahDosen($array_data);
        redirect(base_url('Dosen_controller'));
    }

    public function updateDosenView($niy)
    {
        $data = $this->Dosen_model->getByNiy($niy);
        $data2 = array('data' => $data);
        $this->load->view('dosen/updateDosen_view', $data2);
    }

    public function updateDosen()
    {
        $nama = $this->input->post('nama');
        $niy = $this->input->post('niy');
        $email = $this->input->post('email');


        $array_data = array(
            'nama_dosen' => $nama,
            // 'niy' => $niy,
            'email' => $email,

        );

        $this->Dosen_model->updateDosen($niy, $array_data);
        redirect(base_url('Dosen_controller'));
    }

    public function deleteDosen($niy)
    {
        $this->Link_model->deleteLink_MatkulNiy($niy);
        $this->Dosen_model->deleteDosen($niy);
        redirect(base_url('Dosen_controller'));
    }
}
