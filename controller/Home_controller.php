<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dosen_model');
        $this->load->model('Matkul_model');
        $this->load->model('Link_model');
        $this->load->model('Praktikum_model');
        if (!$this->session->userdata('username')) {
            redirect(base_url(''));
        }
    }

    public function index()
    {
        $data = array(
            'data_dosen' => $this->Dosen_model->getAll(),
            'data_matkul' => $this->Matkul_model->getAll(),
            'data_praktikum' => $this->Praktikum_model->getAll(),
            'data_linkMatkul' => $this->Link_model->getAll(),
            'data_linkPrak' => $this->Link_model->getAllPrak()
        );

        $this->load->view('home', $data);
    }

    public function linkMatkulPage()
    {
        $data = array(
            'data_linkMatkul' => $this->Link_model->getAll(),
        );
        $this->load->view('linkMatkul_view', $data);
    }

    public function linkPrakPage()
    {
        $data = array(
            'data_linkPrak' => $this->Link_model->getAllPrak(),
        );
        $this->load->view('linkPrak_view', $data);
    }
}
