<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Link_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Link_model');
        $this->load->model('Matkul_model');
        $this->load->model('Praktikum_model');
        $this->load->model('Dosen_model');
        if (!$this->session->userdata('username')) {
            redirect(base_url(''));
        }
    }



    public function index()
    {
        $data = array(
            'data' => $this->Link_model->getAll(),
        );
        $this->load->view('link/link_view', $data);
    }

    public function cariLink()
    {
        $namaPrak = $this->input->post('nama_praktikum');
        $data = array(
            'data_linkPrak' => $this->Link_model->getLink($namaPrak),
        );
        $this->load->view('linkPrak_view', $data);
    }
    public function cariLink2()
    {
        $namaMatkul = $this->input->post('nama_matkul');
        $data = array(
            'data_linkMatkul' => $this->Link_model->getLink2($namaMatkul),
        );
        $this->load->view('linkMatkul_view', $data);
    }

    // public function jsonMatkul()
    // {
    //     $data = array(
    //         'data' => $this->Link_model->getAll(),
    //     );
    //     $this->load->view('barang/barang_json', $data);
    // }

    // public function xmlMatkul()
    // {
    //     $data2 = $this->Link_model->getxml();
    //     $data = array('data' => $data2);
    //     // die;
    //     // $this->load->view('barang/barang_xml', $data);
    //     redirect(base_url('xmlBarang.php'));
    // }

    public function viewPraktikum()
    {
        $data = array(
            'data' => $this->Link_model->getAllPrak(),
        );
        $this->load->view('link/linkPraktikum_view', $data);
    }

    public function addLinkMatkulView()
    {
        $data = array(
            'data_matkul' => $this->Matkul_model->getAll(),
            'data_dosen' => $this->Dosen_model->getAll(),
        );
        $this->load->view('link/tambahLinkMatkul_view', $data);
    }


    public function getDataLinkMatkul()
    {
        $nama_matkul = $this->input->post('nama_matkul');
        $nama_dosen = $this->input->post('nama_dosen');
        $kelas = $this->input->post('kelas');
        $link_wa = $this->input->post('link_wa');
        $link_clashroom = $this->input->post('link_clashroom');
        $this->db->where('nama_dosen', $nama_dosen);
        $query = $this->db->get('dosen')->row();

        $this->db->where('nama_matkul', $nama_matkul);
        $query2 = $this->db->get('matakuliah')->row();

        $array_data = array(
            'nama_matkul' => $nama_matkul,
            'nama_dosen' => $nama_dosen,
            'kelas' => $kelas,
            'link_wa' => $link_wa,
            'link_clashroom' => $link_clashroom,
            'niy' => $query->niy,
            'id_matkul' => $query2->id_matkul,

        );

        return $array_data;
    }

    public function addLinkMatkul()
    {

        $this->Link_model->tambahLink_group($this->getDataLinkMatkul());
        redirect(base_url('Link_controller'));
    }

    public function updateLinkView($id_Link)
    {

        $data2 = array(
            'data_link' => $this->Link_model->getById($id_Link),
            'data_matkul' => $this->Matkul_model->getAll(),
            'data_dosen' => $this->Dosen_model->getAll(),

        );

        $this->load->view('link/updateLinkMatkul_view', $data2);
    }

    public function getDataUpdateLinkMatkul()
    {
        $id_Link = $this->input->post('id_link');
        $nama_matkul = $this->input->post('nama_matkul');
        $nama_dosen = $this->input->post('nama_dosen');
        $kelas = $this->input->post('kelas');
        $link_wa = $this->input->post('link_wa');
        $link_clashroom = $this->input->post('link_clashroom');
        $niy = $this->input->post('niy');
        $id_matkul = $this->input->post('id_matkul');

        $array_data = array(
            'nama_matkul' => $nama_matkul,
            'nama_dosen' => $nama_dosen,
            'kelas' => $kelas,
            'link_wa' => $link_wa,
            'link_clashroom' => $link_clashroom,
            'niy' => $niy,
            'id_matkul' => $id_matkul,
            'id_link' => $id_Link,
        );

        return $array_data;
    }
    public function updateLinkMatkul()
    {

        $dataLinkMatkul = $this->getDataUpdateLinkMatkul();

        $this->Link_model->updateLink_matkul($dataLinkMatkul['id_link'], $dataLinkMatkul);
        redirect(base_url('Link_controller'));
    }

    public function deleteLink_Matkul($id_Link)
    {
        $this->Link_model->deleteLink_Matkul($id_Link);
        redirect(base_url('Link_controller'));
    }

    public function deleteLink_MatkulNiy($niy)
    {
        $this->Link_model->deleteLink_MatkulNiy($niy);
        redirect(base_url('Link_controller'));
    }

    //praktikum controller

    public function addLinkPraktikumView()
    {
        $data = array(
            'data_praktikum' => $this->Praktikum_model->getAll(),
        );

        $this->load->view('link/tambahLinkPraktikum_view', $data);
    }

    public function getDataLinkPraktikum()
    {
        $nama_praktikum = $this->input->post('nama_praktikum');
        $slot = $this->input->post('slot');
        $link_wa = $this->input->post('link_wa');
        $link_clashroom = $this->input->post('link_clashroom');

        $this->db->where('nama_praktikum', $nama_praktikum);
        $query2 = $this->db->get('praktikum')->row();

        $array_data = array(
            'nama_praktikum' => $nama_praktikum,
            'slot' => $slot,
            'link_wa' => $link_wa,
            'link_clashroom' => $link_clashroom,
            'id_praktikum' => $query2->id_praktikum,

        );
        return $array_data;
    }
    public function addLinkPraktikum()
    {
        $this->Link_model->tambahLink_prak($this->getDataLinkPraktikum());
        redirect(base_url('Link_controller/viewPraktikum'));
    }

    public function updateLinkPraktikumView($id_Link)
    {

        $data2 = array(
            'data_link' => $this->Link_model->getByIdPrak($id_Link),
            'data_praktikum' => $this->Praktikum_model->getAll(),
        );
        // var_dump($data2);
        // die;
        $this->load->view('link/updateLinkPraktikum_view', $data2);
    }

    public function getDataUpdateLinkPraktikum()
    {
        $id_Link = $this->input->post('id_link');
        $nama_praktikum = $this->input->post('nama_praktikum');
        $slot = $this->input->post('slot');
        $link_wa = $this->input->post('link_wa');
        $link_clashroom = $this->input->post('link_clashroom');
        $id_praktikum = $this->input->post('id_praktikum');

        $array_data = array(
            'nama_praktikum' => $nama_praktikum,
            'slot' => $slot,
            'link_wa' => $link_wa,
            'link_clashroom' => $link_clashroom,
            'id_praktikum' => $id_praktikum,
            'id_link' => $id_Link,
        );

        return $array_data;
    }
    public function updateLinkPraktikum()
    {
        $dataLinkPraktikum = $this->getDataUpdateLinkPraktikum();

        $this->Link_model->updateLink_Prak($dataLinkPraktikum['id_link'], $dataLinkPraktikum);
        redirect(base_url('Link_controller/viewPraktikum'));
    }

    public function deleteLink_Praktikum($id_Link)
    {
        $this->Link_model->deleteLink_prak($id_Link);
        redirect(base_url('Link_controller/viewPraktikum'));
    }
}
