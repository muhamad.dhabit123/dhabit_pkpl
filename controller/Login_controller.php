<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function index()
    {
        // $this->session->sess_destroy();
        $this->load->view('login');
    }

    public function userLogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = $this->db->query("SELECT * FROM user WHERE username = '$username' AND password = '$password'")->row();

        if ($user) {
            $role_id = $user->role_id;

            $data = array(
                'username'  => $username,
                'password'  => $password,
                'role_id'  => $role_id,

            );

            $this->session->set_userdata($data);

            // echo  "<script>alert('Login Berhasil');</script>";
            if ($role_id == 'admin') {
                redirect(base_url('Home_controller'));
            } else {
                redirect(base_url('Home_controller/linkMatkulPage'));
            }
        } else {
            // echo  "<script>alert('Login Gagal');</script>";
            redirect(base_url());
        }
    }

    public function Logout()
    {
        $this->session->sess_destroy();
        redirect(base_url(''));
    }
}
