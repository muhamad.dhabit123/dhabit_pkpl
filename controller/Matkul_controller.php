<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matkul_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Matkul_model');
        $this->load->model('Link_model');
        if (!$this->session->userdata('username')) {
            redirect(base_url(''));
        }
    }

    public function index()
    {
        $data = array(
            'data' => $this->Matkul_model->getAll(),
        );
        // var_dump($data);
        // die;
        $this->load->view('matkul/matkul_view', $data);
    }

    public function addMatkulView()
    {
        $this->load->view('matkul/tambahMatkul_view');
    }

    public function addMatkul()
    {
        $nama_matkul = $this->input->post('nama_matkul');
        $sks = $this->input->post('sks');

        $array_data = array(
            'nama_matkul' => $nama_matkul,
            'sks' => $sks
        );

        $this->Matkul_model->tambahMatakuliah($array_data);
        redirect(base_url('Matkul_controller'));
    }

    public function updateMatkulView($id)
    {
        $data = $this->Matkul_model->getById($id);
        $data2 = array('data' => $data);
        $this->load->view('matkul/updateMatkul_view', $data2);
    }

    public function updateMatkul()
    {
        $nama_matkul = $this->input->post('nama_matkul');
        $sks = $this->input->post('sks');
        $id = $this->input->post('id_matkul');

        $array_data = array(
            'nama_matkul' => $nama_matkul,
            'sks' => $sks
        );

        $this->Matkul_model->updateMatakuliah($id, $array_data);
        redirect(base_url('Matkul_controller'));
    }

    public function deleteMatkul($id_matkul)
    {
        $this->Link_model->deleteLink_MatkulId($id_matkul);
        $this->Matkul_model->deleteMatakuliah($id_matkul);
        redirect(base_url('Matkul_controller'));
    }
}
