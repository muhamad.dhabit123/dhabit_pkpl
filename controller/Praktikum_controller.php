<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Praktikum_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Praktikum_model');
        $this->load->model('Link_model');
        if (!$this->session->userdata('username')) {
            redirect(base_url(''));
        }
    }

    public function index()
    {
        $data = array(
            'data' => $this->Praktikum_model->getAll(),
        );
        $this->load->view('praktikum/praktikum_view', $data);
    }

    public function addPraktikumView()
    {
        $this->load->view('praktikum/tambahPraktikum_view');
    }

    public function addPraktikum()
    {
        $nama_praktikum = $this->input->post('nama_praktikum');
        $sks = $this->input->post('sks');
        $slot = $this->input->post('slot');

        $array_data = array(
            'nama_praktikum' => $nama_praktikum,
            'sks' => $sks,
            'slot' => $slot
        );

        $this->Praktikum_model->tambahPraktikum($array_data);
        redirect(base_url('Praktikum_controller'));
    }

    public function updatepraktikumView($id_praktikum)
    {
        $data = $this->Praktikum_model->getById($id_praktikum);
        $data2 = array('data' => $data);
        $this->load->view('praktikum/updatePraktikum_view', $data2);
    }

    public function updatepraktikum()
    {
        $nama_praktikum = $this->input->post('nama_praktikum');
        $sks = $this->input->post('sks');
        $slot = $this->input->post('slot');
        $id_praktikum = $this->input->post('id_praktikum');

        $array_data = array(
            'nama_praktikum' => $nama_praktikum,
            'sks' => $sks,
            'slot' => $slot
        );

        $this->Praktikum_model->updatePraktikum($id_praktikum, $array_data);
        redirect(base_url('praktikum_controller'));
    }

    public function deletepraktikum($id_praktikum)
    {
        $this->Link_model->deleteLink_prakId($id_praktikum);
        $this->Praktikum_model->deletePraktikum($id_praktikum);
        redirect(base_url('praktikum_controller'));
    }
}
