<?php
// define('BASEPATH') or exit('No direct script access allowed');

class Dosen_model extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('dosen')->result();
    }

    public function getByNiy($niy)
    {
        $this->db->where('niy', $niy);
        return $this->db->get('dosen')->row();
    }

    public function tambahDosen($data)
    {
        $this->db->insert('dosen', $data);
    }

    public function updateDosen($niy, $data)
    {
        $this->db->where('niy', $niy);
        $this->db->update('dosen', $data);
    }

    public function deleteDosen($niy)
    {
        $this->db->where('niy', $niy);
        $this->db->delete('dosen');
    }
}
