<?php
// define('BASEPATH') or exit('No direct script access allowed');

class Link_model extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('link_matkul')->result();
    }

    public function getById($id_link)
    {
        $this->db->where('id_link', $id_link);
        return $this->db->get('link_matkul')->row();
    }

    public function getLink($nama_praktikum)
    {
        $this->db->like('nama_praktikum', $nama_praktikum);
        $query = $this->db->get('link_praktikum');
        return $query->result();
        //$this->db->where('nama_praktikum', $nama_praktikum);
        //return $this->db->get('link_praktikum')->row();
    }

    public function getLink2($nama_matkul)
    {
        $this->db->like('nama_matkul', $nama_matkul);
        $query = $this->db->get('link_matkul');
        return $query->result();
        //$this->db->where('nama_praktikum', $nama_praktikum);
        //return $this->db->get('link_praktikum')->row();
    }

    public function tambahLink_group($data)
    {
        $this->db->insert('link_matkul', $data);
    }

    public function updateLink_matkul($id_link, $data)
    {
        $this->db->where('id_link', $id_link);
        $this->db->update('link_matkul', $data);
    }

    public function deleteLink_Matkul($id_link)
    {
        $this->db->where('id_link', $id_link);
        $this->db->delete('link_matkul');
    }

    public function deleteLink_MatkulNiy($niy)
    {
        $this->db->where('niy', $niy);
        $this->db->delete('link_matkul');
    }

    public function deleteLink_MatkulId($id_matkul)
    {
        $this->db->where('id_matkul', $id_matkul);
        $this->db->delete('link_matkul');
    }

    // model praktikum
    public function getAllPrak()
    {
        return $this->db->get('link_praktikum')->result();
    }

    public function getByIdPrak($id_link)
    {
        $this->db->where('id_link', $id_link);
        return $this->db->get('link_praktikum')->row();
    }

    public function tambahLink_prak($data)
    {
        $this->db->insert('link_praktikum', $data);
    }

    public function updateLink_prak($id_link, $data)
    {
        $this->db->where('id_link', $id_link);
        $this->db->update('link_praktikum', $data);
    }

    public function deleteLink_prak($id_link)
    {
        $this->db->where('id_link', $id_link);
        $this->db->delete('link_praktikum');
    }

    public function deleteLink_prakId($id_praktikum)
    {
        $this->db->where('id_praktikum', $id_praktikum);
        $this->db->delete('link_praktikum');
    }
}
