<?php
// define('BASEPATH') or exit('No direct script access allowed');

class Matkul_model extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('matakuliah')->result();
    }

    public function getById($id_matkul)
    {
        $this->db->where('id_matkul', $id_matkul);
        $query = $this->db->get('matakuliah');
        return $query->row();
    }

    public function tambahMatakuliah($data)
    {
        $this->db->insert('matakuliah', $data);
    }

    public function updateMatakuliah($id_matkul, $data)
    {
        $this->db->where('id_matkul', $id_matkul);
        $this->db->update('matakuliah', $data);
    }

    public function deleteMatakuliah($id_matkul)
    {
        $this->db->where('id_matkul', $id_matkul);
        $this->db->delete('matakuliah');
    }
}
