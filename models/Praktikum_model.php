<?php
// define('BASEPATH') or exit('No direct script access allowed');

class Praktikum_model extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('praktikum')->result();
    }

    public function getById($id_praktikum)
    {
        $this->db->where('id_praktikum', $id_praktikum);
        $query = $this->db->get('praktikum');
        return $query->row();
    }

    public function tambahPraktikum($data)
    {
        $this->db->insert('praktikum', $data);
    }

    public function updatePraktikum($id_praktikum, $data)
    {
        $this->db->where('id_praktikum', $id_praktikum);
        $this->db->update('praktikum', $data);
    }

    public function deletePraktikum($id_praktikum)
    {
        $this->db->where('id_praktikum', $id_praktikum);
        $this->db->delete('praktikum');
    }
}
