<!DOCTYPE html>
<html lang="en">

<head>

    <?php $this->load->view("template/head.php") ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php $this->load->view("template/sidebar.php") ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php // $this->load->view("template/breadcrumb.php") 
                ?>

                <?php $this->load->view("template/navbar.php") ?>
                <!-- Begin Page Content -->

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Dosen_controller'); ?>" style="text-decoration: none;">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-primary text-uppercase mb-1">
                                                    Jumlah Dosen</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                                                                    $no = 0;
                                                                                                    if ($data_dosen != null) {
                                                                                                        foreach ($data_dosen as $d) {
                                                                                                            $no++;
                                                                                                        }
                                                                                                    } else {
                                                                                                        $no = 0;
                                                                                                    }
                                                                                                    echo $no;
                                                                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Matkul_controller'); ?>" style="text-decoration: none;">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-success text-uppercase mb-1">
                                                    Jumlah Matakuliah</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                                                                    $no2 = 0;
                                                                                                    if ($data_matkul != null) {
                                                                                                        foreach ($data_matkul as $d) {
                                                                                                            $no2++;
                                                                                                        }
                                                                                                    } else {
                                                                                                        $no2 = 0;
                                                                                                    }
                                                                                                    echo $no2;
                                                                                                    ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-book fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Praktikum_controller'); ?>" style="text-decoration: none;">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-info text-uppercase mb-1">Jumlah Praktikum
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php
                                                                                                                    $no3 = 0;
                                                                                                                    if ($data_praktikum != null) {
                                                                                                                        foreach ($data_praktikum as $d) {
                                                                                                                            $no3++;
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        $no3 = 0;
                                                                                                                    }
                                                                                                                    echo $no3;
                                                                                                                    ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-tasks fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Link_controller'); ?>" style="text-decoration: none;">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">
                                                    Link Matakuliah</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                                                                    $no4 = 0;
                                                                                                    if ($data_linkMatkul != null) {
                                                                                                        foreach ($data_linkMatkul as $d) {
                                                                                                            $no4++;
                                                                                                        }
                                                                                                    } else {
                                                                                                        $no4 = 0;
                                                                                                    }
                                                                                                    echo $no4;
                                                                                                    ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-table fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Link_controller/viewPraktikum'); ?>" style="text-decoration: none;">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">
                                                    Link Praktikum</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                                                                    $no5 = 0;
                                                                                                    if ($data_linkPrak != null) {
                                                                                                        foreach ($data_linkPrak as $d) {
                                                                                                            $no5++;
                                                                                                        }
                                                                                                    } else {
                                                                                                        $no5 = 0;
                                                                                                    }
                                                                                                    echo $no5;
                                                                                                    ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-table fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <!-- <a href="<?php //echo base_url('Link_controller/viewPraktikum'); 
                                            ?>" style="text-decoration: none;"> -->
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">
                                                Total Link Group</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                                                                                                $no5 = 0;
                                                                                                if ($data_linkPrak != null) {
                                                                                                    foreach ($data_linkPrak as $d) {
                                                                                                        $no5++;
                                                                                                    }
                                                                                                } else {
                                                                                                    $no5 = 0;
                                                                                                }
                                                                                                echo $no5;
                                                                                                ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-table fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Home_controller/linkMatkulPage'); ?>" style="text-decoration: none;">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">
                                                    Preview Link Page</div>
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">Matakuliah</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-table fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <a href="<?php echo base_url('Home_controller/linkPrakPage'); ?>" style="text-decoration: none;">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">
                                                    Preview Link Page</div>
                                                <div class="text-xm font-weight-bold text-warning text-uppercase mb-1">Praktikum</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-table fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php $this->load->view("template/footer.php") ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php $this->load->view("template/scrolltop.php") ?>

    <!-- Logout Modal-->
    <?php $this->load->view("template/modal.php") ?>

    <?php $this->load->view("template/js.php") ?>
</body>

</html>