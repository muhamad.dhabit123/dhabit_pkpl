<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("template/head.php") ?>
</head>

<body>

    <div id="content">


        <?php $this->load->view("template/navbar.php")
        ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <br>

            <div class="container">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Link WA Group dan Clashroom Matakuliah</h1>
                    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                </div>
                <div class="text-left">
                    <p><a href="<?php echo base_url('Home_controller/linkPrakPage') ?>" class="btn btn-primary">Link Group Praktikum</a></p>
                </div>
                <form action="<?php echo base_url('Link_controller/cariLink2') ?>" class="text-right" method="POST">
                    <div class="form-group">
                        <input type="text" name="nama_matkul" class="form-control" style="max-width: 30%;display: inline;" placeholder="cari" required oninvalid="this.setCustomValidity('kata kunci diperlukan')" oninput="this.setCustomValidity('')" value="<?= set_value('nama_matkul') ?>">
                        <input class="btn btn-success" type="submit" name="btn" value="cari">
                    </div>
                </form>
                <table class="table table-striped" style="--table-width: 100%;">
                    <thead>
                        <tr>
                            <td><b>No</td>
                            <td><b>Nama Matakuliah</td>
                            <td><b>Dosen Pengampu</td>
                            <td><b>Kelas</td>
                            <td class="text-center"><b>Link WhatsApp</td>
                            <td class="text-center"><b>Link Classroom</td>

                        </tr>
                    </thead>
                    <?php
                    if ($data_linkMatkul != null) {
                        $count = 0;
                        foreach ($data_linkMatkul as $d) :
                            $count++;
                    ?>
                            <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $d->nama_matkul ?></td>
                                <td><?php echo $d->nama_dosen ?></td>
                                <td><?php echo $d->kelas ?></td>
                                <td class="text-center"><a href="<?php echo $d->link_wa ?>" class="btn btn-success" value="<?php echo $d->link_wa ?>">WhatsApp</a></td>
                                <td class="text-center"><a class="btn btn-primary" value="<?php echo $d->link_clashroom ?>" href="<?php echo $d->link_clashroom ?>">Classroom</input></td>

                            </tr>
                        <?php endforeach;
                    } else { ?>
                        <tr>
                            <td colspan="6" align="center">--- Tidak ada data ---</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- Footer -->
        <?php $this->load->view("template/footer.php") ?>
        <!-- End of Footer -->


        <!-- Scroll to Top Button-->
        <?php $this->load->view("template/scrolltop.php") ?>

        <!-- Logout Modal-->
        <?php $this->load->view("template/modal.php") ?>

        <?php $this->load->view("template/js.php") ?>

</body>

</html>