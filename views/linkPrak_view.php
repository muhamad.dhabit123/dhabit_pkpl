<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("template/head.php") ?>
</head>

<body>
    <div id="content">


        <?php $this->load->view("template/navbar.php") ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <br>

            <div class="container">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Link WA Group dan Clashroom Praktikum</h1>
                    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                </div>
                <div class="text-left">
                    <p><a href="<?php echo base_url('Home_controller/linkMatkulPage') ?>" class="btn btn-primary">Link Group Matakuliah</a></p>
                </div>
                <form action="<?php echo base_url('Link_controller/cariLink') ?>" class="text-right" method="POST">
                    <div class="form-group">
                        <input type="text" name="nama_praktikum" class="form-control" style="max-width: 30%;display: inline;" placeholder="cari" required oninvalid="this.setCustomValidity('kata kunci diperlukan')" oninput="this.setCustomValidity('')" value="<?= set_value('nama_praktikum') ?>">
                        <input class="btn btn-success" type="submit" name="btn" value="cari">
                    </div>
                </form>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td><b>No</td>
                            <td><b>Nama Praktikum</td>
                            <td><b>Slot</td>
                            <td class="text-center"><b>Link WhatsApp</td>
                            <td class="text-center"><b>Link Clashroom</td>
                        </tr>
                    </thead>
                    <?php
                    if ($data_linkPrak != null) {
                        $count = 0;
                        foreach ($data_linkPrak as $d) :
                            $count++;
                    ?>
                            <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $d->nama_praktikum ?></td>
                                <td><?php echo $d->slot ?></td>
                                <td class="text-center"><a href="#" class="btn btn-success" value="<?php echo $d->link_clashroom ?>">WhatsApp</a></td>
                                <td class="text-center"><a class="btn btn-primary" href="#">Clashroom</input></td>

                            </tr>
                        <?php endforeach;
                    } else { ?>
                        <tr>
                            <td colspan="6" align="center">--- Tidak ada data ---</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- Footer -->
        <?php $this->load->view("template/footer.php") ?>
        <!-- End of Footer -->


        <!-- Scroll to Top Button-->
        <?php $this->load->view("template/scrolltop.php") ?>

        <!-- Logout Modal-->
        <?php $this->load->view("template/modal.php") ?>

        <?php $this->load->view("template/js.php") ?>

</body>

</html>